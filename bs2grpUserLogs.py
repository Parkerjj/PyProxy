# coding=utf-8
# coding=utf-8
from google.appengine.ext import db
from google.appengine.api import memcache
from email import utils
from time import mktime
import datetime
import logging
from bs2grpfile import *


class BS2GRPUserLogs(db.Model):
    ipAddress = db.StringProperty(required=True)
    requests = db.ListProperty(db.Blob)

    def __init__(self, ipAddress):
        self.ipAddress = ipAddress
        self.requests = []

    def addNewRequest(self, mdatetime, queryString):
        logging.info('Append new request:   queryString : %s   mdatatime : %s'%(queryString,datetime_to_string(mdatetime)))
        newrequest = UserRequest(mdatetime, queryString)
        self.requests.append(newrequest)

    def selfCheckCanAccess(self):
        blockusers = memcache.get('BlockUsersSet')
        if not blockusers:
            blockusers = set()
            memcache.add(key='BlockUsersSet', value=blockusers, time=3600*24)

        if self.ipAddress in blockusers:
            return False

        timeLimit = datetime.datetime.now() - datetime.timedelta(minutes=60)
        queryset = set()

        for request in self.requests:
            queryset.add(request.queryString)
            if request.mdatetime < timeLimit:
                self.requests.remove(request)
        if self.requests:
            logging.info('IP Address : ' + self.ipAddress + '    Request Count : %d' % len(self.requests))
            ret = (120 - len(self.requests)) > 0
            if not ret:
                #120个记录里少于20个相同词,恶意用户
                if len(queryset) < 20:
                    blockusers.add(self.ipAddress)
                    memcache.set('BlockUsersSet', blockusers)

            return ret
        logging.info('IP Address : ' + self.ipAddress + '    No Request,Count : %d' % len(self.requests))
        return True


class UserRequest(db.Model):
    mdatetime = db.DateTimeProperty(required=True)
    queryString = db.StringProperty(required=False)

    def __init__(self, dt, qs):
        self.mdatetime = dt
        self.queryString = qs
